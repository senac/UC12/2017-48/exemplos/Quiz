/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.quiz.servlet;

import br.com.senac.quiz.dao.JogadorDAO;
import br.com.senac.quiz.model.Jogador;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author sala304b
 */
@WebServlet(name = "JogadorServlet", urlPatterns = {"/SalvarJogador"})
public class JogadorServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        int id = Integer.parseInt(request.getParameter("id"));
        String nome = request.getParameter("nome");        
        String sobreNome = request.getParameter("sobrenome");        
        int NumeroJogo = Integer.parseInt(request.getParameter("NumeroJogo"));        
        
        Jogador jogador = new Jogador();        
        jogador.setId(id);
        jogador.setNome(nome);
        jogador.setSobreNome(sobreNome);
        jogador.setNumeroJogo(NumeroJogo);
        
        JogadorDAO dao = new JogadorDAO();
        String mensagem  ; 
        
        if (jogador.getId() == 0) {
            dao.inserir(jogador);
            mensagem = "Salvo com sucesso!";
        } else {
            dao.atualizar(jogador);
            mensagem = "Alterado com sucesso!";
        }
        
        request.setAttribute("jogador", jogador);
        request.setAttribute("mensagem", mensagem);
        
        RequestDispatcher view = request.getRequestDispatcher("./Jogador.jsp");
        
        view.forward(request, response);
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
