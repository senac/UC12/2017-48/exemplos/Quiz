package br.com.senac.quiz.dao;

import br.com.senac.quiz.model.Jogador;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JogadorDAO implements DAO<Jogador> {

    @Override
    public List<Jogador> getLista() {
        List<Jogador> lista = new ArrayList<>();
        Connection connection = Conexao.getConnection();
        try {
            String query = "SELECT * FROM jogador;";
            PreparedStatement ps = connection.prepareStatement(query);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Jogador jogador = new Jogador();
                jogador.setId(rs.getInt("id"));
                jogador.setNome(rs.getString("nome"));
                jogador.setSobreNome(rs.getString("sobrenome"));
                jogador.setNumeroJogo(rs.getInt("numeroJogo"));
                lista.add(jogador);
            }

        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

        return lista;
    }

    @Override
    public Jogador getPorId(int id) {
        Jogador jogador = null;
        Connection connection = Conexao.getConnection();
        try {
            String query = "SELECT * FROM jogador where id = ?;";
            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            if (rs.first()) {
                jogador = new Jogador();
                jogador.setId(rs.getInt("id"));
                jogador.setNome(rs.getString("nome"));
                jogador.setSobreNome(rs.getString("sobrenome"));
                jogador.setNumeroJogo(rs.getInt("numeroJogo"));
            }

        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

        return jogador;
    }

    @Override
    public void inserir(Jogador jogador) {
        Connection connection = Conexao.getConnection();
        try {
            String query = "Insert into jogador (nome , sobrenome , numeroJogo) "
                    + "values ( ? , ? , ? );";

            PreparedStatement ps = connection.prepareStatement(query,
                    PreparedStatement.RETURN_GENERATED_KEYS);

            ps.setString(1, jogador.getNome());
            ps.setString(2, jogador.getSobreNome());
            ps.setInt(3, jogador.getNumeroJogo());

            int linhas = ps.executeUpdate();

            if (linhas == 1) {

                ResultSet rs = ps.getGeneratedKeys();
                rs.first();
                int id = rs.getInt(1);
                jogador.setId(id);

            }

        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }
    }

    @Override
    public void atualizar(Jogador jogador) {
        Connection connection = Conexao.getConnection();
        try {
            String query = "update jogador set "
                    + "nome = ? , "
                    + "sobrenome = ? , "
                    + "numeroJogo = ? "
                    + "where id = ?; ";

            PreparedStatement ps = connection.prepareStatement(query);

            ps.setString(1, jogador.getNome());
            ps.setString(2, jogador.getSobreNome());
            ps.setInt(3, jogador.getNumeroJogo());
            ps.setInt(4, jogador.getId());

            ps.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }
    }

    @Override
    public void delete(int id) {

        Connection connection = Conexao.getConnection();
        try {
            String query = "delete from jogador where id = ?";

            PreparedStatement ps = connection.prepareStatement(query);
            ps.setInt(1, id);
            ps.executeUpdate();

        } catch (SQLException ex) {
            System.out.println("Erro executar a query ....");
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println("erro eo desconectar");
            }
        }

    }

    public static void main(String[] args) {

        testarSelectTodos();

    }

    public static void testarInsercao() {

        Jogador jogador = new Jogador();
        jogador.setNome("Daniel2");
        jogador.setSobreNome("Santiago2");
        jogador.setNumeroJogo(3);

        JogadorDAO dao = new JogadorDAO();
        dao.inserir(jogador);

        System.out.println(jogador);
    }

    public static void testarAlteracao() {
        Jogador jogador = new Jogador();
        jogador.setId(1);
        jogador.setNome("Jose");
        jogador.setSobreNome("santos");
        jogador.setNumeroJogo(5);

        JogadorDAO dao = new JogadorDAO();
        dao.atualizar(jogador);

        System.out.println(jogador);
    }

    public static void testarExclusao() {
        int id = 1;
        JogadorDAO dao = new JogadorDAO();
        dao.delete(id);

        System.out.println("Deletado com sucesso...");
    }

    public static void testarSelectTodos() {
        JogadorDAO dao = new JogadorDAO();
        
        List<Jogador> lista = dao.getLista();
        
        for(Jogador j : lista){
            System.out.println(j);
        }
        
        
    }

    public static void testarSelectPorId() {
        int id = 2;
        JogadorDAO dao = new JogadorDAO();
        Jogador jogador = dao.getPorId(id);

        System.out.println(jogador);

    }

}
