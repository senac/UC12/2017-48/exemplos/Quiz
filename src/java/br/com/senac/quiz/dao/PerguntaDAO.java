package br.com.senac.quiz.dao;

import br.com.senac.quiz.model.Pergunta;
import br.com.senac.quiz.model.Resposta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PerguntaDAO implements DAO<Pergunta> {

    @Override
    public List<Pergunta> getLista() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void inserir(Pergunta pergunta) {

        Connection connection = Conexao.getConnection();

        try {
            connection.setAutoCommit(false);

            String queryInsertPergunta = "INSERT INTO pergunta "
                    + "(Pergunta) VALUES (?);";

            PreparedStatement ps
                    = connection.prepareStatement(queryInsertPergunta,
                            PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, pergunta.getPergunta());
            System.out.println(ps);
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            rs.first();
            int idPergunta = rs.getInt(1);
            pergunta.setId(idPergunta);

            String queryResposta = "INSERT INTO resposta "
                    + "(Resposta, Letra, Correta, idPergunta) "
                    + "VALUES (?,?,?,?);";
            ps = connection.
                    prepareStatement(
                            queryResposta,
                            PreparedStatement.RETURN_GENERATED_KEYS
                    );

            for (Resposta r : pergunta.getRespostas()) {
                ps.setString(1, r.getResposta());
                ps.setString(2, r.getLetra());
                ps.setBoolean(3, r.isCorreta());
                ps.setInt(4, idPergunta);

                ps.executeUpdate();
                rs = ps.getGeneratedKeys();
                rs.first();
                int idResposta = rs.getInt(1);
                r.setId(idResposta);

//                if (r.getLetra().equals("C")) {
//                    throw new RuntimeException("Falhou !!!!!");
//                }

            }
            
            connection.commit();
            

        } catch (Exception ex) {
            
            try { 
                connection.rollback();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
            
            ex.printStackTrace();
        }finally{
            try {
                connection.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public static void main(String[] args) {

        Pergunta pergunta = new Pergunta();
        pergunta.setPergunta("Qual o dia de Hoje ?");

        Resposta a = new Resposta();
        a.setCorreta(true);
        a.setLetra("A");
        a.setResposta("21");

        Resposta b = new Resposta();
        b.setCorreta(false);
        b.setLetra("B");
        b.setResposta("22");

        Resposta c = new Resposta();
        c.setCorreta(false);
        c.setLetra("C");
        c.setResposta("44");

        Resposta d = new Resposta();
        d.setCorreta(false);
        d.setLetra("D");
        d.setResposta("28");

        Resposta e = new Resposta();
        e.setCorreta(false);
        e.setLetra("E");
        e.setResposta("1");

        pergunta.adicionaResposta(a);
        pergunta.adicionaResposta(b);
        pergunta.adicionaResposta(c);
        pergunta.adicionaResposta(d);
        pergunta.adicionaResposta(e);

        PerguntaDAO dao = new PerguntaDAO();

        dao.inserir(pergunta);

    }

    @Override
    public void atualizar(Pergunta objeto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Pergunta getPorId(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
