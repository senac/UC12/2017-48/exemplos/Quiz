package br.com.senac.quiz.model;

import java.util.ArrayList;
import java.util.List;

public class Pergunta {

    private int id;
    private String pergunta;
    private List<Resposta> respostas;
    
    public Pergunta(){
        this.respostas = new ArrayList<>();
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPergunta() {
        return pergunta;
    }

    public void setPergunta(String pergunta) {
        this.pergunta = pergunta;
    }

    public List<Resposta> getRespostas() {
        return respostas;
    }

    public void setRespostas(List<Resposta> respostas) {
        this.respostas = respostas;
    }

    public Resposta getRespostaCorreta() {

        if (respostas != null) {
            for (Resposta r : this.respostas) {
                if (r.isCorreta()) {
                    return r;
                }
            }
        }

        return null;
    }

    public void adicionaResposta(Resposta a) {
        
        if(this.respostas.size() < 6){
            this.respostas.add(a) ; 
        }else{
            throw  new RuntimeException("Somente são permitidas 5 respostas.");
        }
        
        
    }

}
