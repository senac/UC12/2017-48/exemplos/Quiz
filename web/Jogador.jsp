<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="br.com.senac.quiz.model.Jogador"%>
<jsp:include page="header.jsp"/>

<jsp:useBean id="jogador" class="br.com.senac.quiz.model.Jogador" scope="request" />
<jsp:useBean id="mensagem" class="java.lang.String" scope="request" /> 

<c:if test="${mensagem ne ''}" >
    <div class="alert alert-success">
        <strong>Sucesso!</strong> ${mensagem}
    </div>
</c:if>


<form method="post" action="./SalvarJogador">

    <input type="hidden" name="id" value="${jogador.id}" />

    <div class="row">
        <div class="col-md-2 col-lg-4" ></div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-4">
            <h2>Cadastro de Jogador</h2>
        </div>
        <div class="col-md-2 col-lg-4"></div>
    </div>


    <div class="form-group row ">
        <div class="col-md-2 col-lg-4" ></div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-4">
            <label for="nome" >Nome:</label><input class="form-control" id="nome" type="text" name="nome" value="${jogador.nome}" />
        </div>
        <div class="col-md-2 col-lg-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-md-2 col-lg-4" ></div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-4">
            <label for="sobrenome" >Sobrenome:</label><input class="form-control" id="sobrenome" type="text" name="sobrenome" value="${jogador.sobreNome}" />
        </div>
        <div class="col-md-2 col-lg-4"></div>
    </div>
    <div class="form-group row">
        <div class="col-md-2 col-lg-4" ></div>
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-4">
            <label for="NumeroJogo">Numero Jogo:</label><input class="form-control" type="text" id="NumeroJogo" name="NumeroJogo" value="${jogador.numeroJogo}" />
        </div>
        <div class="col-md-2 col-lg-4"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-2 col-lg-4" ></div>
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-4">
        <input type="submit" value="Iniciar Jogo !" class="btn btn-primary float-left" />
    </div>
    <div class="col-md-2 col-lg-4"></div>
</div>
</form>

<jsp:include page="footer.jsp"/>



