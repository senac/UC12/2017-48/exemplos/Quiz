<%-- 
    Document   : usuario
    Created on : 21/03/2018, 21:11:35
    Author     : sala304b
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="usuario" class="br.com.senac.quiz.model.Usuario" scope="request">
            <jsp:setProperty name="usuario" property="*"/>
        </jsp:useBean>
         
        <form method="post" action="./salvarUsuario">
            <input type="hidden" value="${usuario.id}"  name="id"/>
            <label>Nome:</label> <input type="text" value="${usuario.nome}" name="nome" /> <br />
            <label>Senha:</label> <input type="text" value="${usuario.senha}" name="senha" /> <br />
            <label>Apelido:</label> <input type="text" value="${usuario.apelido}" name="apelido" /> <br />
            <input type="submit" value="Enviar" />
        </form>
        
        
    </body>
</html>
