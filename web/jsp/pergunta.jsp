<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp"/>

<jsp:useBean id="pergunta" class="br.com.senac.quiz.model.Pergunta" scope="request"  />


<form method="post" action="./SalvarPergunta">
    <div class="form-group row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
            <label for="pergunta" class="col-form-label">Pergunta</label>
            <input type="text" class="form-control" id="pergunta" name="pergunta" value="${pergunta.pergunta}" />
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">

        </div>
    </div>

    <c:forEach var="resposta" items="${pergunta.respostas}">

        <div class="form-group row">

            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                <label for="resposta">Resposta ${resposta.letra}</label>
                <input type="text" class="form-control" id="resposta" name="resposta" value="${resposta.resposta}" />
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
                <div class="radio" style="margin-top: 40px;">
                    <label><input  type="radio" name="correta" value="${resposta.letra}">Correta</label>
                </div>
            </div>

        </div>

    </c:forEach>

    <div class="form-group row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>

    </div>
</form>



<jsp:include page="footer.jsp" />



