<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title>Quiz</title>
    </head>
    <body>


        <% if (request.getAttribute("mensagem") != null) {
                
                out.print(request.getAttribute("mensagem"));
            }
        %>

        <div class="container">
            <form action="./Login" method="post">

                <h2 >Log in</h2>       
                <div class="form-group">
                    <input type="text"  placeholder="Usuário"  name="nome" class="form-control">
                </div>
                <div>
                    <input type="password"  placeholder="Senha" name="senha" >
                </div>
                <div >
                    <button type="submit" >Log in</button>
                </div>
                <div >
                    <label><input type="checkbox"> Manter conectado</label>
                    <a href="#" >Esqueceu a senha ?</a>
                </div>        
            </form>
            <p ><a href="#">Criar uma conta</a></p>
        </div>


 
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>