<jsp:include page="header.jsp"/>

<form method="post" action="./SalvarPergunta">
    <div class="form-group row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
            <label for="pergunta" class="col-form-label">Pergunta</label>
            <input type="text" class="form-control" id="pergunta" />
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">

        </div>
    </div>

    <div class="form-group row">

        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
            <label for="resposta">Resposta A</label>
            <input type="text" class="form-control" id="resposta" />
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-6">
            <div class="radio" style="margin-top: 40px;">
                <label><input  type="radio" name="correta" value="A">Correta</label>
            </div>
        </div>

    </div>
    <div class="form-group row">

        <div class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
            <label for="resposta">Resposta B</label>
            <input type="text" class="form-control" id="resposta" />
        </div>
        <div class="col-xs-12 col-sm-8 col-md-4 col-lg-6">
            <div class="radio" style="margin-top: 40px;">
                <label><input  type="radio" name="correta" value="B">Correta</label>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
            <label for="resposta">Resposta C</label>
            <input type="text" class="form-control" id="resposta" />
        </div>
        <div class="col-xs-12 col-sm-8 col-md-4 col-lg-6">
            <div class="radio" style="margin-top: 40px;">
                <label><input  type="radio" name="correta" value="C">Correta</label>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
            <label for="resposta">Resposta D</label>
            <input type="text" class="form-control" id="resposta" />
        </div>
        <div class="col-xs-12 col-sm-8 col-md-4 col-lg-6">
            <div class="radio" style="margin-top: 40px;">
                <label><input  type="radio" name="correta" value="D">Correta</label>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-xs-12 col-sm-4 col-md-8 col-lg-6">
            <label for="resposta">Resposta E</label>
            <input type="text" class="form-control" id="resposta" />
        </div>
        <div class="col-xs-12 col-sm-8 col-md-4 col-lg-6">
            <div class="radio" style="margin-top: 40px;">
                <label><input  type="radio" name="correta" value="E">Correta</label>
            </div>
        </div>

    </div>

    <div class="form-group row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>

    </div>
</form>



<jsp:include page="footer.jsp" />



